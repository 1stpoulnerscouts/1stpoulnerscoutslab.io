---
layout: post
title: "Ringwood Rabbit Run"
date: 2017-02-21 12:00:00
img: /img/rrr.jpg
categories: news
active: news
---


<img src="/img/rrr.jpg" class="img-responsive pull-right img-text" />
<p>The Ringwood Rabbit Run is back! Enjoy off-road running on tracks and trails. Easter treats for runners on completion!</p>
<ul>
	<li>2nd April 2017</li>
	<li>Moors Valley Country Park</li>
	<li>5k starts at 9.30 am and 10k starts and 10 am</li>
	<li>£1 parking for the morning!</li>
	<li>Warm up by David Lloyd Ringwood.</li>
</ul>
<br>
<p><a href="https://ringwoodrabbitrun.eventbrite.co.uk" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-share"></span> Register here</a></p>
